extends Node

class_name Faction

# Player faction is the default
@export var faction : int = 1
@export var colour : Color = Color(1,1,1,1)

func _init(faction_: int, colour_: Color):
	faction = faction_
	colour = colour_

func is_same_faction(p_faction : Faction):
	return faction == p_faction.faction
