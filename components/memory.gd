extends Node

class_name Memory

@export var mydrone : Drone

# TODO: turn slots into a dictionary with memory_id as the key instead of the
# array it is now
var memory_store : Dictionary
var last_memory_id: int = 0
var max_memory_size :int = 3
var memory_size : int = 0
var priority_list : Dictionary = {
	"healing" : ["friend", 5],
	"building" : ["foe", 2],
	"drone" : ["foe", 1],
}

# Memory slot
class Slot:
	var priority: int
	var group: StringName
	# Friend or foe
	var type: StringName
	var position: Vector2
	var memory_id: int
	var data: String = ""

	func _init(priority_, group_, type_, position_, memory_id_):
		priority = priority_
		group = group_
		type = type_
		position = position_
		memory_id = memory_id_

			#priority = [tuple[1], group, tuple[0], body.global_position]
# TODO: Remove memories if something is not there any more
# Not sure how to do this though, can hardly check all positions in the
# area2d everytime. Could store position by tilemap location to make it
# a bit more grid based..
# Otherwise could only do this for buildings and have an invisible
# marker for each destroyed building, that when the drone encounters it
# causes it to evaluate its memory

func encounter(body : PhysicsBody2D) -> void:
	var info = get_priority_info(body)
	if not info:
		return

	# Check if already in memory
	if in_memory(info):
		return

	var compare = memory_store.get(info.priority)
	# Memory not full
	if memory_size < max_memory_size:
		if not compare:
			memory_store[info.priority] = [info]
		else:
			memory_store[info.priority].append(info)

		memory_size += 1
		return

	# Memory full
	var keys = memory_store.keys()
	keys.sort()
	# If priority lower than lowest in memory do nothing
	if info.priority < keys[0]:
		return

	# Remove oldest at lowest priority
	memory_store[keys[0]].remove_at(0)
	if (memory_store[keys[0]].size() == 0):
		memory_store.erase(keys[0])
	# TODO: move to separate function?
	if not compare:
		memory_store[info.priority] = [info]
	else:
		memory_store[info.priority].append(info)

func in_memory(info: Slot) -> bool:
	var compare = memory_store.get(info.priority)
	if not compare:
		return false

	# If only there was a has custom
	for x in compare:
		if info.group == x.group and info.type == x.type and info.position == x.position:
			return true
	return false

func is_foe(body : PhysicsBody2D) -> bool:
	if body.faction:
		return not mydrone.faction.is_same_faction(body.faction)
	return false

func to_display():
	if memory_store.size() > 0:
		var arr = []
		for key in memory_store.keys():
			for slot in memory_store[key]:
				arr.append([slot.group, slot.type, slot.position])
		return arr
	return null

func erase(slot: Slot) -> void:
	for key in memory_store.keys():
		# NOTE: We can't return here, because this slot might be in another
		# priority key
		memory_store[key].erase(slot)

func find(slot: Slot) -> Slot:
	for key in memory_store.keys():
		for slt in memory_store[key]:
			if slt == slot:
				return slot
	return null

func find_latest_by(group: StringName, type: StringName) -> Slot:
	return find_latest_custom(func(slot) -> bool: return slot.group == group and slot.type == type)

func find_latest_custom(callable: Callable) -> Slot:
	for key in memory_store.keys():
		# Latest are at the end so go in reverse
		var slots = memory_store[key]
		for slot in slots.slice(-1, -slots.size() - 1, -1):
			if callable.call(slot):
				return slot
	return null



func get_priority_info(body : PhysicsBody2D) -> Slot:
	var priority: int = -1
	# Return null if groups not at all in the priority list
	var slot: Slot = null
	var type = "friend"
	if is_foe(body):
		type = "foe"

	# Check our priority list for the groups and assign the highest priority
	for group in body.get_groups():
		var tuple = priority_list.get(group)
		if tuple and type == tuple[0] and tuple[1] > priority:
			priority = tuple[1]
			slot = Slot.new(priority, group, tuple[0], body.global_position, last_memory_id)
			last_memory_id += 1

	return slot
