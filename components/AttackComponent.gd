extends Node

class_name AttackComponent

@export var attack_range : float = 75.0
@export var attack_rate : float = 0.5

@export var Projectile : PackedScene

var last_attack_time : float

func attack(origin : CharacterBody2D, _target : CharacterBody2D):
	var cur_time = Time.get_unix_time_from_system()
	# TODO: Only shoot when target is (roughly) straight ahead
	if cur_time - last_attack_time > attack_rate:
		# TODO: Check it can take damage
		last_attack_time = cur_time
		var projectile = Projectile.instantiate()
		# Detach it from the unit (otherwise position is relative to that) and
		# the drone turning would cause the projectile to turn
		get_node("/root").add_child(projectile)
		projectile.global_position = origin.global_position
		projectile.rotation = origin.rotation
		projectile.global_position += projectile.transform.x * 18

func in_range(global_position, target_global_position):
	var dist = global_position.distance_to(target_global_position)
	# TODO: Check it can take damage
	return dist <= attack_range
