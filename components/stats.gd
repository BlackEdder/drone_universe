extends Node

class_name Stats

@export var max_health : float
@export var health : float

signal on_health_change
signal on_damage
signal on_death

func heal(amount : float) -> void:
	health += amount
	health = clamp(health, 0, max_health)
	on_health_change.emit()

func increase_max_health(amount : int) -> void:
	max_health += amount
	max_health = clamp(max_health, 0, max_health)
	on_health_change.emit()

func take_damage(damage_to_take) -> void:
	heal(-damage_to_take)

	if health <= 0:
		on_death.emit()
		return

	on_damage.emit()
