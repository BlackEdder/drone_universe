extends CanvasLayer

@onready var unit_state : GridContainer = get_node("Status/VBoxContainer/UnitState")
@onready var upgrades : VBoxContainer = get_node("Status/VBoxContainer/Upgrades")
@onready var extra_info : VBoxContainer = get_node("Status/VBoxContainer/ExtraInfo")
@onready var game_manager = get_node("/root/Main")
@onready var status: ColorRect = $Status
@onready var resource_label : Label = get_node("StatusBar/HBoxContainer/Resource")
@onready var build_queue : VBoxContainer = get_node("BuildQueue/VBoxContainer")

var selected : PhysicsBody2D
signal on_deselect

func _ready() -> void:
	game_manager.on_resource.connect(_draw_resource)
	_draw_build_queue()

func _draw_build_queue() -> void:
	# TODO: Split setup from the drawing
	while not game_manager or not game_manager.player:
		await get_tree().create_timer(0.1).timeout
	if not game_manager.player.on_build_queue.is_connected(_draw_build_queue):
		game_manager.player.on_build_queue.connect(_draw_build_queue)

	_clear(build_queue)
	for build in game_manager.player.build_queue:
		var button : Button = Button.new()
		button.text = build
		build_queue.add_child(button)
		# button.toggle_mode = true
		# button.button_pressed = false
		#
		# # Add button action
		# var f = func ():
		# 	if (selected != null):
		# 		selected.upgrade_manager.add_upgrade(behaviour)
		#
		# button.connect("pressed", f)



func _draw_resource(resource) -> void:
	resource_label.text = str(round(resource))

func close_status_window() -> void:
	if is_instance_valid(selected):
		on_deselect.emit()

	status.visible = false
	selected = null

func open_status_window(body : PhysicsBody2D) -> void:
	# Could already be connected to another body
	if is_instance_valid(selected):
		on_deselect.emit()

	status.visible = true
	selected = body

	if selected and selected.has_node("Stats"):
		selected.stats.on_health_change.connect(_draw_stats)
		on_deselect.connect(func():
			selected.stats.on_health_change.disconnect(_draw_stats), CONNECT_ONE_SHOT
		)
	_draw_stats()
	_draw_faction()
	_draw_upgrades()
	_draw_extra_info()

func _draw_stats() -> void:
	if selected and selected.has_node("Stats"):
		unit_state.visible = true
		unit_state.get_node("Health").text = str(round(selected.stats.health)) + "/" + str(round(selected.stats.max_health))
	else:
		unit_state.visible = false

func _draw_faction() -> void:
	if selected and selected.faction:
		unit_state.get_node("Faction").modulate = selected.faction.colour

func _clear(container):
	for n in container.get_children():
		container.remove_child(n)
		n.queue_free()

func _draw_extra_info() -> void:
	if selected and "health_pool" in selected:
		_clear(extra_info)
		var label : Label = Label.new()
		label.text = " Health Pool: " + str(round(selected.health_pool)) + "/" + str(round(selected.max_health_pool))
		extra_info.add_child(label)
		extra_info.visible = true
		if not selected.on_health_pool_change.is_connected(_draw_extra_info):
			selected.on_health_pool_change.connect(_draw_extra_info)
			on_deselect.connect(func():
				selected.on_health_pool_change.disconnect(_draw_extra_info), CONNECT_ONE_SHOT
			)

	elif selected.has_node("Memory"):
		_clear(extra_info)
		# var arr = selected.memory.to_display()
		# print(arr)
		var grid = _array_to_grid(selected.memory.to_display())
		extra_info.add_child(grid)

		# TODO: ideally this should not be hardcoded
		var grid_brain = _array_to_grid(selected.brain.to_display(), selected.brain.root_state._active_state)
		extra_info.add_child(grid_brain)
		extra_info.visible = true

	else:
		extra_info.visible = false

func _array_to_grid(nested, sel = null) -> GridContainer:
	var grid = GridContainer.new()
	# TODO: Find out why is_instance_valid does not seem to work here
	if not nested or nested.size() == 0:
		return grid
	grid.columns = nested[0].size()
	for arr in nested:
		for elem in arr:
			var label : Label = Label.new()
			label.text = str(elem)
			if elem == sel:
				label.text += "*"
			grid.add_child(label)

	return grid

func _draw_upgrades() -> void:
	if not is_instance_valid(selected) or not selected.has_node("UpgradeManager"):
		return

	_clear(upgrades)

	# Add available upgrades
	for behaviour in selected.upgrade_manager.available_upgrades:
		var button : Button = Button.new()
		button.text = behaviour
		button.toggle_mode = true
		button.button_pressed = false

		# Add button action
		var f = func ():
			if (selected != null):
				selected.upgrade_manager.add_upgrade(behaviour)

		button.connect("pressed", f)
		upgrades.add_child(button)

func _on_healing_building_pressed():
	game_manager.player.build_queue_append("healing_building")

func _on_spawn_building_pressed():
	game_manager.player.build_queue_append("spawn_building")
