extends Camera2D

const move_speed = 50
const zoom_amount = 0.2

# Dragging the screen
var mouse_start_pos
var dragging = false


func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_WHEEL_UP and event.pressed:
			zoom.x += zoom_amount
			zoom.y += zoom_amount
		elif event.button_index == MOUSE_BUTTON_WHEEL_DOWN and event.pressed:
			zoom.x -= zoom_amount
			zoom.y -= zoom_amount
		elif event.button_index == MOUSE_BUTTON_RIGHT and event.pressed:
			mouse_start_pos = event.position
			dragging = true
		else:
			dragging = false

	elif event is InputEventMouseMotion and dragging:
		position += 0.75 * (mouse_start_pos - event.position)
		mouse_start_pos = event.position


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_key_pressed(KEY_LEFT):
		position.x += -move_speed * delta
	if Input.is_key_pressed(KEY_RIGHT):
		position.x += move_speed * delta
	if Input.is_key_pressed(KEY_UP):
		position.y += -move_speed * delta
	if Input.is_key_pressed(KEY_DOWN):
		position.y += move_speed * delta
