extends Node

class_name Brain

@export var drone: Drone
@export var the_states: Array[State]
@export var root_state: CompoundState

var states: Dictionary

enum States { WANDER, HEAL, RETREAT, CHASEATTACK }


func _ready() -> void:
	states[States.WANDER] = [0, _wander_condition]
	states[States.HEAL] = [3, _healing_condition]
	states[States.RETREAT] = [-1, _retreat_condition]
	states[States.CHASEATTACK] = [2, _attack_condition]

func decide() -> void:
	var priority = -1
	var behaviour = -1
	for key in states.keys():
		var value = states[key]
		if value[0] > priority and value[1].call():
			priority = value[0]
			behaviour = key

	if behaviour >= 0:
		# print("Choosing:" + str(the_states[behaviour]))
		# TODO: Use _handle_transition, because currently break history nodes
		# var trans = Transition.new()
		# trans.to = the_states[behaviour].get_path()
		# root_state._handle_transition(trans, the_states[0])
		root_state._active_state._state_exit()
		root_state._active_state = the_states[behaviour]
		root_state._active_state._state_enter()
	else:
		print("No usable behaviour")


func _wander_condition() -> bool:
	# Fall back, always returns true
	return true


func _attack_condition() -> bool:
	return drone.enemies_in_sight().size() > 0


func _healing_condition() -> bool:
	if drone.stats.health <= 0.75 * drone.stats.max_health:
		var slot = drone.memory.find_latest_custom(func(slot):
			return slot.group == &"healing" and slot.type == &"friend" and slot.data == "")
		if slot:
			drone.healing_slot = slot
			return true

	return false


func _retreat_condition() -> bool:
	return drone.stats.health <= 0.25 * drone.stats.max_health


func to_display():
	return the_states.map(func(v) -> Array[State]: return [v])
