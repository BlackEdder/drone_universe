extends Node2D

# Should get a starting location
var starting_position: Vector2
var game_manager: GameManager
var faction : Faction

var build_queue: Array[String]
signal on_build_queue

func random_location() -> Vector2:
	var direction = Vector2(randf_range(-50, 50), randf_range(-50, 50))
	return starting_position + direction

func _physics_process(_delta: float) -> void:
	# Not set yet
	if not game_manager:
		return

	# Check if I can afford building (GameManager will know)!
	if build_queue.is_empty() or not game_manager.enough_funds_for(faction, build_queue.front()):
		return

	# Can tell game maker to build spawn
	var pos = random_location()
	if game_manager.try_build(faction, build_queue.front(), pos):
		build_queue.pop_front()
		on_build_queue.emit()

# TODO: Can we do this with setter for the array?
func build_queue_append(build: String) -> void:
	build_queue.append(build)
	on_build_queue.emit()
