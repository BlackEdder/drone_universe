extends StaticBody2D
class_name SpawnBuilding

var drone_scene = preload("res://scenes/drone.tscn")
var faction : Faction:
	set(value):
		faction = value
		sprite.self_modulate = faction.colour
	get:
		return faction

@onready var stats : Stats = $Stats
@onready var sprite = $Sprite2D
@onready var selection_visual : Sprite2D = $SelectionVisual

func _ready() -> void:
	stats.max_health = 250
	stats.health = 250

	stats.on_damage.connect(
		func():
			sprite.modulate = Color.RED
			await get_tree().create_timer(0.1).timeout
			sprite.modulate = Color.WHITE
	)
	stats.on_death.connect(queue_free)

func _on_timer_timeout():
	_spawn_drone()

func toggle_selection_visual (toggle):
	selection_visual.visible = toggle

func _spawn_drone():
	var drone = drone_scene.instantiate() as Drone
	add_child(drone)
	drone.set_global_position(global_position)
	drone.faction = faction
