class_name Drone
extends CharacterBody2D

@export var max_speed = 50.0
@export var acceleration = 50.0

@onready var nav_agent: NavigationAgent2D = $NavigationAgent2D
@onready var sprite: Sprite2D = $Sprite

var faction : Faction:
	set(value):
		faction = value
		sprite.self_modulate = faction.colour
	get:
		return faction

@onready var stats = $Stats
@onready var memory = $Memory
@onready var brain = $Brain
@onready var sensor = $Sensor
# TODO: Use RayShape so we can adjust for the width of the projectile?
@onready var raycast: RayCast2D = get_node("RayCast2D")
@onready var attack_component: AttackComponent = $AttackComponent

@onready var game_manager: GameManager = get_node("/root/Main")
@onready var selection_visual : Sprite2D = get_node("SelectionVisual")
@onready var upgrade_manager : UpgradeManager = $UpgradeManager
@onready var state_chart:StateChart = get_node("Brain/StateChart")

var patrol_distance : float = 150.0

var target : CharacterBody2D

func _ready() -> void:
	stats.max_health = 100
	stats.health = 100

	stats.on_damage.connect(
		func():
			sprite.modulate = Color.RED
			await get_tree().create_timer(0.1).timeout
			sprite.modulate = Color.WHITE
			state_chart.send_event("decision")
	)

	stats.on_death.connect(queue_free)

func enemies_in_sight() -> Array:
	return sensor.get_overlapping_bodies().filter(func(body : PhysicsBody2D) -> bool:
			if body.is_in_group("drone"):
				return !faction.is_same_faction(body.faction) and line_of_sight(body.global_position)
			return false)

func line_of_sight(global_pos : Vector2) -> bool:
	raycast.target_position = raycast.to_local(global_pos)
	raycast.force_raycast_update()
	return !raycast.is_colliding()

func _acquire_target() -> bool:
	for unit in enemies_in_sight():
		set_target(unit)
		return true

	return false

func toggle_selection_visual (toggle):
	selection_visual.visible = toggle

func known_behaviours() -> Array[String]:
	return upgrade_manager.known

func _death():
	queue_free()

func move_to_location (location):
	target = null
	nav_agent.target_position = location

func set_target (new_target):
	target = new_target

func _move_to_random_position(distance : float):
	var dir = Vector2(randf_range(-1, 1), randf_range(-1, 1)).normalized() * distance
	move_to_location(global_position + dir)
	while not nav_agent.is_target_reachable():
		dir = Vector2(randf_range(-1, 1), randf_range(-1, 1)).normalized() * distance
		move_to_location(global_position + dir)

func _on_wander_state_physics_processing(delta: float) -> void:
	if nav_agent.is_navigation_finished():
		_move_to_random_position(patrol_distance)
	_on_move_state_physics_processing(delta)

func _on_retreat_state_processing(delta: float) -> void:
	if nav_agent.is_navigation_finished() or stats.health >= 0.5 * stats.max_health:
		state_chart.send_event("decision")
	else:
		_on_move_state_physics_processing(delta)

func _on_retreat_state_entered() -> void:
	_move_to_random_position(150)

var healing_slot
func _on_healing_state_entered() -> void:
	# TODO: Should we use compound state for wander towards
	if not healing_slot:
		print("Fatal error, should have checked if existed first")
	move_to_location(healing_slot.position)

func clear_healing_data(slot) -> void:
	if not slot:
		print("Why are things so wrong!")
	await get_tree().create_timer(5.0).timeout
	slot.data = ""

func _on_healing_state_physics_processing(delta: float) -> void:
	# FIXME: if healing pool almost empty, stop healing and
	# 1. Look for other building in memory or
	# 2. set priority of healing to low (temporary)
	for node in sensor.get_overlapping_bodies():
		if node.is_in_group("healing") and node.is_in_group("building"):
			if node.empty_health_pool() or stats.health == stats.max_health:
				if node.is_connected_to(self):
					node.stop_healing(self)
				state_chart.send_event("decision")
				if healing_slot and node.empty_health_pool():
					healing_slot.data = "empty"
					call_deferred("clear_healing_data", healing_slot)
				healing_slot = null
				return
			elif !node.in_range(self):
				move_to_location(node.global_position)
				_on_move_state_physics_processing(delta)
				return
			elif !node.is_connected_to(self):
				node.start_healing(self)
				return

	if nav_agent.is_navigation_finished():
		# TODO: figure out why this doesn't work
		# print("Can't find the healing building")
		# memory.erase(healing_slot)
		# healing_slot = null
		# Note that this should automatically get us going to the next one if we know one
		state_chart.send_event("decision")
	healing_slot = null
	_on_move_state_physics_processing(delta)

func _on_sensor_body_entered(body : PhysicsBody2D) -> void:
	memory.encounter(body)
	if (body.is_in_group("drone") and !faction.is_same_faction(body.faction)):
		state_chart.send_event("decision")

func _on_sensor_body_exited(body : PhysicsBody2D) -> void:
	memory.encounter(body)

func _on_decision_state_physics_processing(_delta: float):
	brain.decide()

func _on_move_state_physics_processing(delta: float) -> void:
	if nav_agent.is_navigation_finished():
		return
	var pos = nav_agent.get_next_path_position()
	var direction = global_position.direction_to(pos)
	velocity += direction.normalized() * acceleration * delta
	velocity = velocity.limit_length(max_speed)
	rotation = velocity.angle()

	var collision_info = move_and_collide(velocity * delta)
	# TODO: Make bouncing work
	# if collision_info:
	# 	velocity = velocity.bounce(collision_info.get_normal())
	if collision_info and target == null:
		_move_to_random_position(50)

var follow_target: CharacterBody2D
var follow_range: float
func _on_chase_and_attack_state_entered() -> void:
	# TODO: acquire target could/should return a target
	# TODO: Should acquire target be a state?
	if not _acquire_target():
		state_chart.send_event("decision")
	follow_target = target
	follow_range = attack_component.attack_range


func _on_follow_state_physics_processing(delta: float) -> void:
	# TODO: Implement
	if not is_instance_valid(follow_target):
		state_chart.send_event("no_target")
		return
	var dist = global_position.distance_to(follow_target.global_position)
	if dist < follow_range:
		state_chart.send_event("goal_reached")
	else:
		move_to_location(follow_target.global_position)
		_on_move_state_physics_processing(delta)

func _on_fire_state_physics_processing(delta: float) -> void:
	# TODO: Implement
	# Check whether in line of sight and in range
	if is_instance_valid(follow_target):
		if !line_of_sight(follow_target.global_position):
			follow_target = null
		else:
			var dist = global_position.distance_to(follow_target.global_position)
			if dist > follow_range:
				state_chart.send_event("out_of_range")

	if not is_instance_valid(follow_target):
		state_chart.send_event("no_target")
		return

	var angle = self.to_local(follow_target.global_position).angle()
	var turn_speed = 3
	rotation += sign(angle) * min(turn_speed * delta, abs(angle))

	attack_component.attack(self, follow_target)
