extends TileMap

class_name MapReference

@export var source_id: int = 0
@export var layer: int = 0
@export var width: int = 256
@export var height: int = 196
@export var wall_tile_id = { "coords" = Vector2i(1,0), "alternative_id" = 0 }

# Used throughout the algorithm
var store = {}
var tile_ref_map = {}
var tiles : Array[TileReference] = []

class TileReference:
	var atlas_coords: Vector2i
	var alternative_tile: int
	# Shouldn't really be here, but it makes it easy
	var probability: int

	func _init(atlas_coords_, alternative_tile_):
		atlas_coords = atlas_coords_
		alternative_tile = alternative_tile_

# Work out which ones work together on each side (big lookup table), each tile right left top bottom
# create this table by looping over each pair (don't forget that the a tile can have themself as neighbour)
# Draw them given probability and working together starting from center and progressing
# randomly
# Work out how to access the probability (part of tiledata actually)

# Helper mappings
const cell_neighbour_map = {
	# NOTE: Order inside the areas are important, because we just check the peering bits match in that order
	# TODO: Understand when we need CORNER here and when SIDE, think it depends on if map is set to side and corners or only sides etc.
	Vector2i.LEFT: [TileSet.CELL_NEIGHBOR_TOP_LEFT_CORNER, TileSet.CELL_NEIGHBOR_LEFT_SIDE, TileSet.CELL_NEIGHBOR_BOTTOM_LEFT_CORNER],
	Vector2i.RIGHT: [TileSet.CELL_NEIGHBOR_TOP_RIGHT_CORNER, TileSet.CELL_NEIGHBOR_RIGHT_SIDE, TileSet.CELL_NEIGHBOR_BOTTOM_RIGHT_CORNER],
	Vector2i.UP: [TileSet.CELL_NEIGHBOR_TOP_LEFT_CORNER, TileSet.CELL_NEIGHBOR_TOP_SIDE, TileSet.CELL_NEIGHBOR_TOP_RIGHT_CORNER],
	Vector2i.DOWN: [TileSet.CELL_NEIGHBOR_BOTTOM_LEFT_CORNER, TileSet.CELL_NEIGHBOR_BOTTOM_SIDE, TileSet.CELL_NEIGHBOR_BOTTOM_RIGHT_CORNER]
}
const directions = [Vector2i.LEFT, Vector2i.RIGHT, Vector2i.UP, Vector2i.DOWN]
const opposites = {
	Vector2i.LEFT: Vector2i.RIGHT,
	Vector2i.RIGHT: Vector2i.LEFT,
	Vector2i.UP: Vector2i.DOWN,
	Vector2i.DOWN: Vector2i.UP
}

func valid_tiles(pos: Vector2i) -> Array:
	var init: bool = false
	var selected_tiles = tiles.duplicate()
	for direction in directions:
		# For each direction check if a tile is there and if so, find the ones we can use
		var coord = pos + direction
		var alt_id = get_cell_alternative_tile(layer, coord)
		if alt_id == -1:
			continue
		var tile_ref = tile_ref_map[get_cell_atlas_coords(layer, coord)][alt_id]
		var other_tiles = store[tile_ref][opposites[direction]]
		if not init:
			selected_tiles = other_tiles
			init = true
			continue
		# Keep only the ones we already know
		selected_tiles = selected_tiles.filter(func(tref):
			return other_tiles.has(tref))
	return selected_tiles

func get_peering_bits(tile, bits):
	return bits.map(func(bit):
			return tile.get_terrain_peering_bit(bit)
	)

func peering_bits_match(tile1: TileData, tile2: TileData, direction) -> bool:
	return get_peering_bits(tile1, cell_neighbour_map[direction]) == get_peering_bits(tile2, cell_neighbour_map[opposites[direction]])

func pick_one(array: Array) -> TileReference:
	if array.is_empty():
		return null
	var sum = array.reduce(func(acc, x):
		return acc + x.probability, 0)

	var acc = 0
	var w = randf_range(0, sum)
	for x in array:
		acc += x.probability
		if acc >= w:
			return x

	print("Failure in pick one method")
	return null

func set_terrain_cell(coords):
	# Check if there is already a tile there
	if get_cell_alternative_tile(layer, coords) == -1:
		var vtiles = valid_tiles(coords)
		var tref = pick_one(vtiles)
		if tref:
			set_cell(layer, coords, source_id, tref.atlas_coords, tref.alternative_tile)
		else:
			print("No matching tile found")



# Called when the node enters the scene tree for the first time.
func _ready():
	draw_outline(TileReference.new(wall_tile_id["coords"], wall_tile_id["alternative_id"]))
	print("Info dump from the generator")
	var source = tile_set.get_source(source_id)
	var vec = source.get_atlas_grid_size()
	for i in vec.x:
		for j in vec.y:
			for k in source.get_next_alternative_tile_id(Vector2i(i, j)):
				var tile_ref = TileReference.new(Vector2i(i, j), k)
				# Alternative ids can be skipped (if they were removed at some point?)
				# so check for existence
				# TODO: This throws a warning if alternative tile does not exist, but I have not found
				# another way to get all tiles
				var tile = source.get_tile_data(tile_ref.atlas_coords, tile_ref.alternative_tile)
				if tile:
					tile_ref.probability = tile.probability
					tiles.append(tile_ref)
					# TODO: bit annoyting to have to store the ref twice. Would be ideal to cleanup
					if not tile_ref_map.has(tile_ref.atlas_coords):
						# Do we need this, or does gdscript make keys automatically
						tile_ref_map[tile_ref.atlas_coords] = {}
					tile_ref_map[tile_ref.atlas_coords][tile_ref.alternative_tile] = tile_ref
				else:
					print("Ignoring missing alternative id. You can safely ignore the godot error above")
	for i in tiles.size():
		var tr0 = tiles[i]
		store[tr0] = {}
		for direction in directions:
			store[tr0][direction] = []
			# Go in reverse to be sure the key exists in store
			for j in range(i, -1, -1):
				var tr1 = tiles[j]
				var tile1 = source.get_tile_data(tr0.atlas_coords, tr0.alternative_tile)
				var tile2 = source.get_tile_data(tr1.atlas_coords, tr1.alternative_tile)
				if peering_bits_match(tile1, tile2, direction):
					# Add tr0 and tr1 to lookup table and the other way around
					store[tr0][direction].append(tr1)
					var dir2 = opposites[direction]
					if not store[tr1].has(dir2):
						store[tr1][dir2] = []
					store[tr1][dir2].append(tr0)
	call_deferred("draw_terrain")

func draw_outline(wall_tile: TileReference) -> void:
	# Draw cell: void set_cell ( int layer, Vector2i coords, int source_id=-1, Vector2i atlas_coords=Vector2i(-1, -1), int alternative_tile=0 )
	# set_cell(0, Vector2i(0, 0), 0, Vector2i(0, 0), 0)
	for i in width:
		set_cell(layer, Vector2i(i, 0), source_id, wall_tile.atlas_coords, wall_tile.alternative_tile)
		set_cell(layer, Vector2i(i, height - 1), source_id, wall_tile.atlas_coords, wall_tile.alternative_tile)
	for j in height:
		set_cell(layer, Vector2i(0, j), source_id, wall_tile.atlas_coords, wall_tile.alternative_tile)
		set_cell(layer, Vector2i(width - 1, j), source_id, wall_tile.atlas_coords, wall_tile.alternative_tile)

func draw_terrain():
	for i in width:
		for j in height:
			set_terrain_cell(Vector2i(i, j))
