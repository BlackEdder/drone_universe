extends StaticBody2D
class_name HealthBuilding

var faction : Faction:
	set(value):
		faction = value
		sprite.self_modulate = faction.colour
	get:
		return faction

@onready var stats : Stats = $Stats
@onready var sprite : Sprite2D = $Sprite2D
@onready var selection_visual : Sprite2D = $SelectionVisual

signal on_health_pool_change
var max_health_pool : float = 100
var health_pool : float = max_health_pool

var max_healing_distance : int = 100

var connected_drones : Array[CharacterBody2D]

func _ready() -> void:
	stats.max_health = 250
	stats.health = 250

	stats.on_damage.connect(
		func():
			sprite.modulate = Color.RED
			await get_tree().create_timer(0.1).timeout
			sprite.modulate = Color.WHITE
	)
	stats.on_death.connect(queue_free)

func _process(delta):
	var prev_health_pool = round(health_pool)
	health_pool += delta
	health_pool = clamp(health_pool, 0, max_health_pool)
	_heal_connected_drones(delta)

	if prev_health_pool != round(health_pool):
		on_health_pool_change.emit()

func start_healing(drone : Drone) -> void:
	if faction.is_same_faction(drone.faction):
		connected_drones.append(drone)

func is_connected_to(drone : Drone) -> bool:
	return connected_drones.has(drone)

func stop_healing(drone : Drone) -> void:
	connected_drones.remove_at(connected_drones.find(drone))

func toggle_selection_visual (toggle):
	selection_visual.visible = toggle

func in_range(drone : Drone) -> bool:
	var dist = global_position.distance_to(drone.global_position)
	return dist < max_healing_distance

func sufficient_health_pool() -> bool:
	return health_pool > 10

func empty_health_pool() -> bool:
	return health_pool < 1

func _heal_connected_drones(delta) -> void:
	# Auto disconnect
	connected_drones = connected_drones.filter(func(drone) -> bool:
		if not is_instance_valid(drone):
			return false
		return self.in_range(drone))

	for drone in connected_drones:
		if drone and is_instance_valid(drone) and health_pool > 0:
			var hp = clamp(10 * delta, 0, health_pool)
			health_pool -= hp
			drone.stats.heal(hp)
