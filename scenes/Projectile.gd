extends Area2D

class_name Projectile

@export var damage: int = 10
@export var speed = 50
@export var total_distance = 125


func _physics_process(delta):
	var dist = speed * delta
	position += transform.x * dist
	total_distance -= dist
	if total_distance <= 0:
		queue_free()


func _on_body_entered(body: Node2D):
	if body.has_node("Stats"):
		body.stats.take_damage(damage)

	queue_free()
