class_name UpgradeManager
extends Node

# Probably should be known_behaviours only?
# Or should we also include a count of the number of increase_healths we hold
var available_upgrades: Array[String] = ["increase_max_health", "heal"]

@export var drone: Drone

# Signal fired when new behaviours are added
signal on_behaviour_change


func add_upgrade(upgrade: String):
	if upgrade == "increase_max_health":
		_apply_increase_max_health(10)
		# Also heals
		_apply_increase_health(10)
	elif upgrade == "heal":
		_apply_increase_health(1)
	else:
		print("Unknown upgrade: " + upgrade)
		print("Drone faction: " + str(drone.faction))


func _apply_increase_max_health(amount: int):
	drone.stats.increase_max_health(amount)


func _apply_increase_health(amount: int):
	drone.stats.heal(amount)
