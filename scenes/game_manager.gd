extends Node2D

class_name GameManager

@export var width: int = 500
@export var height: int = 300
@export var building_cost: float = 100

var resources: Dictionary
@onready var npc = $NPC
@onready var player = $Player

signal on_resource

# TODO: Do we actually need to keep track of selected_unit?
var selected_unit: PhysicsBody2D

var drone_scene = preload("res://scenes/drone.tscn")
var healing_building_scene = preload("res://scenes/healing_building.tscn")
var spawn_building_scene = preload("res://scenes/spawn_building.tscn")
@onready var shapecast: ShapeCast2D = $ShapeCast2D
@onready var ui: CanvasLayer = get_node("/root/Main/UI")

func can_be_placed_here(global_pos: Vector2):
	shapecast.set_global_position(global_pos)
	shapecast.force_shapecast_update()
	return !shapecast.is_colliding()

func enough_funds_for(faction: Faction, type: String) -> bool:
	if type == "spawn_building" or type == "healing_building":
		return resources[faction.faction] >= building_cost
	print("Unknown building: ", type)
	return false

func try_build(faction: Faction, type: String, pos: Vector2):
	if not enough_funds_for(faction, type):
		return false
	if not can_be_placed_here(pos):
		return false

	var scene
	if type == "spawn_building":
		scene = spawn_building_scene.instantiate()
	elif type == "healing_building":
		scene = healing_building_scene.instantiate()
	else:
		print("Unknown building: ", type)
		return false

	resources[faction.faction] -= building_cost
	if faction.is_same_faction(player.faction):
		on_resource.emit(resources[faction.faction])
	add_child(scene)
	scene.global_position = pos
	scene.faction = faction
	# TODO: This should be done by scene itself, for example have a setter for faction
	# that does it
	return true

func start_game():
	# TODO: More permanent solution than hacky timer
	# Short pause to give the map time to be drawn
	await get_tree().create_timer(0.1).timeout
	var player_faction = Faction.new(1, Color(1, 1, 1, 1))
	var computer_faction = Faction.new(2, Color(1, 1, 0, 1))

	player.starting_position = Vector2(randi_range(0, width) - width / 2.0, randi_range(0, height) - height / 2.0)
	player.game_manager = self
	player.faction = player_faction

	resources[player_faction.faction] = 200
	resources[computer_faction.faction] = 200
	npc.starting_position = Vector2(randi_range(0, width) - width / 2.0, randi_range(0, height) - height / 2.0)
	npc.game_manager = self
	npc.faction = computer_faction

func _ready():
	call_deferred("start_game")

func _physics_process(delta: float):
	for key in resources:
		var prev = round(resources[key])
		resources[key] += delta
		if key == player.faction.faction and round(resources[key]) != prev:
			on_resource.emit(resources[key])

func _unhandled_input(event):
	if event is InputEventMouseButton and event.pressed:
		if event.button_index == MOUSE_BUTTON_LEFT:
			_try_select_unit()

func _get_selected_unit():
	var space = get_world_2d().direct_space_state
	var query = PhysicsPointQueryParameters2D.new()
	query.position = get_global_mouse_position()
	var intersection = space.intersect_point(query, 1)

	if !intersection.is_empty():
		return intersection[0].collider

	return null


func _try_select_unit():
	var unit = _get_selected_unit()

	if unit != null and unit is PhysicsBody2D:
		_select_unit(unit)
	else:
		_unselect_unit()


func _select_unit(unit):
	_unselect_unit()
	selected_unit = unit
	selected_unit.toggle_selection_visual(true)
	ui.open_status_window(unit)


func _unselect_unit():
	if selected_unit != null:
		selected_unit.toggle_selection_visual(false)

	selected_unit = null
	ui.close_status_window()
