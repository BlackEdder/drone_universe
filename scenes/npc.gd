extends Node

# Should get a starting location
var starting_position: Vector2
var game_manager: GameManager
var faction : Faction

func random_location() -> Vector2:
	var direction = Vector2(randf_range(-50, 50), randf_range(-50, 50))
	return starting_position + direction

func _physics_process(_delta: float) -> void:
	# Not set yet
	if not game_manager:
		return

	# Check if I can afford building (GameManager will know)!
	if not game_manager.enough_funds_for(faction, "spawn_building"):
		return

	# Can tell game maker to build spawn
	var pos = random_location()
	game_manager.try_build(faction, "spawn_building", pos)
